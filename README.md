# Code Coverage Server #

### What is this repository for? ###
Code Coverage is a java/web application built on top of JACOCO library. Published under [LGPLv3.0](http://www.gnu.org/licenses/lgpl-3.0.html) license.

* Version: 1.0

### How do I get set up? ###

* Check out repository in Eclipse as a Java/Maven project  
* View [Code Coverage Server App](https://bitbucket.org/wishcoder/coverageserverapp
/wiki/Home) wiki for more details

### Who do I talk to? ###
Ajay Singh [message4ajay@gmail.com]