package us.wishcoder.coverage.server.context;

import java.net.URL;
import java.security.ProtectionDomain;


import org.eclipse.jetty.webapp.WebAppContext;

public class AppContextBuilder {
	private WebAppContext webAppContext;

	public WebAppContext buildWebAppContext(String contextPath) {
		webAppContext = new WebAppContext();
		webAppContext.setDescriptor(webAppContext + "/WEB-INF/web.xml");
		webAppContext.setResourceBase(".");

		webAppContext.setContextPath(contextPath);
		webAppContext.setParentLoaderPriority(true);

		ProtectionDomain protectionDomain = AppContextBuilder.class
				.getProtectionDomain();
		URL location = protectionDomain.getCodeSource().getLocation();
		webAppContext.setWar(location.toExternalForm());

		return webAppContext;
	}
}
