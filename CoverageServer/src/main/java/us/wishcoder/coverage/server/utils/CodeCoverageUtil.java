package us.wishcoder.coverage.server.utils;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.DefaultLogger;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

public class CodeCoverageUtil {
	private static Logger logger = Logger.getLogger(CodeCoverageUtil.class);

	private static final String CODE_COVERAGE_BUILD_XML = "build.coverage.xml";
	private static final String BUILD_TARGET_MERGE = "target.merge";
	private static final String BUILD_TARGET_REPORT = "target.report";

	public CodeCoverageUtil() {
	}

	public static boolean executeMergeTask(String userName, String rootPath,
			String coveragePath, String coverageFiles, String coverageMainFile) {
		boolean success = false;
		DefaultLogger consoleLogger = getConsoleLogger();

		// prepare Ant project
		Project project = new Project();
		project.setUserProperty("app.root.path", rootPath);
		project.setUserProperty("app.coverage.path", coveragePath);
		project.setUserProperty("app.coverage.files", coverageFiles);
		project.setUserProperty("app.coverage.main.file", coverageMainFile);

		// capture events for Ant script build start/stop/failure
		try {

			InputStream initialStream = CodeCoverageUtil.class
					.getResourceAsStream(CODE_COVERAGE_BUILD_XML);
			byte[] buffer = new byte[initialStream.available()];
			initialStream.read(buffer);

			File buildFile = new File(CODE_COVERAGE_BUILD_XML);
			OutputStream outStream = new FileOutputStream(buildFile);
			try {
				outStream.write(buffer);
			} catch (Exception exception) {
				String msg = "[" + userName + "] Unable to write file "
						+ CODE_COVERAGE_BUILD_XML;
				logger.error(msg);
				project.fireBuildFinished(exception);
				throw new RuntimeException(msg, exception);
			} finally {
				if (outStream != null) {
					outStream.close();
				}
				outStream = null;
			}

			project.setUserProperty("ant.file", buildFile.getAbsolutePath());
			project.addBuildListener(consoleLogger);

			project.fireBuildStarted();
			project.init();

			ProjectHelper projectHelper = ProjectHelper.getProjectHelper();
			project.addReference("ant.projectHelper", projectHelper);
			projectHelper.parse(project, buildFile);

			// if no target specified then default target will be executed
			project.executeTarget(BUILD_TARGET_MERGE);
			project.fireBuildFinished(null);
			success = true;
		} catch (BuildException | IOException exception) {
			String msg = "[" + userName
					+ "] Unable to execute merge task due to the error: "
					+ exception.getMessage();
			logger.error(msg);
			project.fireBuildFinished(exception);
			throw new RuntimeException(msg, exception);
		}
		return success;
	}

	public static boolean executeReportTask(String userName, String rootPath,
			String reportTitle, String coverageMainFile,
			String coverageReportPath, String coverageReportOutputPath,
			String classesJars, String sourceJars) {
		boolean success = false;
		DefaultLogger consoleLogger = getConsoleLogger();

		// prepare Ant project
		Project project = new Project();
		project.setUserProperty("app.root.path", rootPath);
		project.setUserProperty("app.coverage.report.title", reportTitle);
		project.setUserProperty("app.coverage.main.file", coverageMainFile);
		project.setUserProperty("app.coverage.report.path", coverageReportPath);
		project.setUserProperty("app.coverage.report.output.path",
				coverageReportOutputPath);
		project.setUserProperty("app.coverage.report.classes.jars", classesJars);
		project.setUserProperty("app.coverage.report.source.jars", sourceJars);

		// capture events for Ant script build start/stop/failure
		try {
			InputStream initialStream = CodeCoverageUtil.class
					.getResourceAsStream(CODE_COVERAGE_BUILD_XML);
			byte[] buffer = new byte[initialStream.available()];
			initialStream.read(buffer);

			File buildFile = new File(CODE_COVERAGE_BUILD_XML);
			OutputStream outStream = new FileOutputStream(buildFile);
			try {
				outStream.write(buffer);
			} catch (Exception exception) {
				String msg = "[" + userName + "] Unable to write file "
						+ CODE_COVERAGE_BUILD_XML;
				logger.error(msg);
				project.fireBuildFinished(exception);
				throw new RuntimeException(msg, exception);
			} finally {
				if (outStream != null) {
					outStream.close();
				}
				outStream = null;
			}

			project.setUserProperty("ant.file", buildFile.getAbsolutePath());
			project.setBasedir(rootPath);
			project.addBuildListener(consoleLogger);

			project.fireBuildStarted();
			project.init();

			ProjectHelper projectHelper = ProjectHelper.getProjectHelper();
			project.addReference("ant.projectHelper", projectHelper);
			projectHelper.parse(project, buildFile);

			// if no target specified then default target will be executed
			project.executeTarget(BUILD_TARGET_REPORT);
			project.fireBuildFinished(null);
			success = true;
		} catch (BuildException | IOException exception) {
			String msg = "[" + userName
					+ "] Unable to execute report task due to the error: "
					+ exception.getMessage();
			logger.error(msg);
			project.fireBuildFinished(exception);
			throw new RuntimeException(msg, exception);
		}

		return success;
	}

	private static DefaultLogger getConsoleLogger() {
		DefaultLogger consoleLogger = new DefaultLogger();
		consoleLogger.setErrorPrintStream(System.err);
		consoleLogger.setOutputPrintStream(System.out);
		consoleLogger.setMessageOutputLevel(Project.MSG_INFO);
		return consoleLogger;
	}
}
