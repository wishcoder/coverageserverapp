package us.wishcoder.coverage.server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;

public class JettyServer {
	
	private Server server;
	private int webServerPort;

	public JettyServer(int webServerPort) {
		this.webServerPort = webServerPort;
		server = new Server(webServerPort);
	}

	public void setHandler(ContextHandlerCollection contexts) {
		server.setHandler(contexts);
	}

	public void destroyServer() {
		if (server != null)
			server.destroy();
	}

	public void start() throws Exception {
		server.start();
	}

	public void stop() throws Exception {
		server.stop();
		server.join();
	}

	public boolean isStarted() {
		return server.isStarted();
	}

	public boolean isStopped() {
		return server.isStopped();
	}

	public Server getServer() {
		return server;
	}

	public int getPort() {
		return webServerPort;
	}
}
