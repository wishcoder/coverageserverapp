package us.wishcoder.coverage.server.schema;


import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User implements Serializable{
	private static final long serialVersionUID = -4439506915859050471L;

	@Id
	@GeneratedValue
	@Column(name = "user_id")
	private Long userId;
	
	@Column(name = "user_name", length=32, unique=true, nullable=false)
	private String userName;

	@Column(name = "user_password", length=32, nullable=false)
	private String userPassword;
	
	@OneToMany(mappedBy="projectUser")
	private Set<Project> projects;

	@OneToMany(mappedBy="coverageUser")
	private Set<Coverage> coverage;

	@OneToMany(mappedBy="reportUser")
	private Set<Report> reports;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="manager_id")
	private User manager;
	
	@OneToMany(mappedBy="manager")
	private Set<User> subordinates = new HashSet<User>();
		
	@Column(name = "active")
	private Boolean active;	
		
	@Column(name = "date_modified")
	private Date dateModified;
	
	public User(){
		super();
		this.dateModified = new Date(System.currentTimeMillis());
	}	
	
	public User(String userName, String userPassword){
		this(userName, userPassword, true);
	}	
	
	public User(String userName, String userPassword, Boolean active){
		this();
		this.userName = userName;
		this.userPassword = userPassword;
		this.active = active;
	}
	
	public Long getUserId(){
		return this.userId;
	}
	
	public void setUserId(Long userId){
		this.userId = userId;
	}
	
	public String getUserName(){
		return this.userName;
	}
	
	public void setUserName(String userName){
		this.userName= userName;
	}	
	
	public String getUserPassword(){
		return this.userPassword;
	}
	
	public void setUserPassword(String userPassword){
		this.userPassword= userPassword;
	}
	
	public Set<Project> getProjects(){
		return this.projects;
	}	
	
	public void setProjects(Set<Project> projects){
		this.projects = projects;
	}
	
	public Set<Coverage> getCoverage(){
		return this.coverage;
	}	
	
	public void setCoverage(Set<Coverage> coverage){
		this.coverage = coverage;
	}
	
	public Set<Report> getReports(){
		return this.reports;
	}	
	
	public void setReports(Set<Report> reports){
		this.reports = reports;
	}
	
	public User getManager(){
		return this.manager;
	}	
	
	public void setManager(User manager){
		this.manager = manager;
	}	

	public Set<User> getSubordinates(){
		return this.subordinates;
	}	
	
	public void setSubordinates(Set<User> subordinates){
		this.subordinates = subordinates;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getDateModified(){
		return this.dateModified;	
	}	
	
	public void setDateModified(Date dateModified){
		this.dateModified = dateModified;	
	}
	
	public boolean isSuperAdmin(){
		if(this.getManager() != null && this.getManager().getUserId() == this.getUserId()){
			return true;
		} else {
			return false;
		}		
	}
	
	public boolean isAdmin(){
		if(this.getManager() == null || this.getManager().getUserId() == null){
			return true;
		} else {
			return false;
		}		
	}
}




