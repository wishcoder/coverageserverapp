package us.wishcoder.coverage.server.schema;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "REPORT")
public class Report{
	@Id
	@GeneratedValue
	@Column(name = "report_id")
	private Long reportId;
	
	@Column(name = "report_name", length=256, unique=true, nullable=false)
	private String reportName;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User reportUser;

	@ManyToOne
	@JoinColumn(name="project_id")
	private Project reportProject;
		
	@Column(name = "date_modified")
	private Date dateModified;
	
	public Report(){
		super();
		this.dateModified = new Date(System.currentTimeMillis());
	}	
	
	public Report(String reportName){
		this();
		this.reportName = reportName;
	}	
	
	public Long getReportId(){
		return this.reportId;
	}
	
	public void setReportId(Long reportId){
		this.reportId = reportId;
	}
	
	public String getReportName(){
		return this.reportName;
	}
	
	public void setReportName(String reportName){
		this.reportName= reportName;
	}	
	
	public User getReportUser(){
		return this.reportUser;
	}	
	
	public void setReportUser(User reportUser){
		this.reportUser = reportUser;
	}	
	
	public Project getReportProject(){
		return this.reportProject;
	}	
	
	public void setReportProject(Project reportProject){
		this.reportProject = reportProject;
	}
	
	public Date getDateModified(){
		return this.dateModified;	
	}	
	
	public void setDateModified(Date dateModified){
		this.dateModified = dateModified;	
	}
}




