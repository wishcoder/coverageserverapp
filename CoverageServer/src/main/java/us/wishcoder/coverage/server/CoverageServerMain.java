package us.wishcoder.coverage.server;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;

import us.wishcoder.coverage.server.context.AppContextBuilder;
import us.wishcoder.coverage.server.ui.ServerRunnerUI;

public class CoverageServerMain extends JPanel {
	private static final long serialVersionUID = 5270642420125771007L;
	private static Logger logger = Logger.getLogger(CoverageServerMain.class);
	

	public static final String APP_CONTEXT_PATH = "/coverageserver";
	public static final String APP_TITLE = "Code Coverage Server";
	private static final int WEBSERVER_PORT = 8080;
	
	private static ServerRunnerUI serverRunnerUI;
	private static final CoverageServerMain appPanel = new CoverageServerMain();
	
	public static void main(String[] args) {
		try {
			// UIManager.setLookAndFeel(l&f);
			ContextHandlerCollection contexts = new ContextHandlerCollection();
			contexts.setHandlers(new Handler[] { new AppContextBuilder()
					.buildWebAppContext(APP_CONTEXT_PATH) });

			final JettyServer jettyServer = new JettyServer(Integer.getInteger("serverport", WEBSERVER_PORT));
			contexts.setServer(jettyServer.getServer());
			jettyServer.setHandler(contexts);

			serverRunnerUI = new ServerRunnerUI(appPanel, jettyServer, APP_TITLE);
			serverRunnerUI.addWindowListener(new WindowAdapter() {
				@Override
				public void windowIconified(WindowEvent e) {
					serverRunnerUI.setVisible(false);
				}

				@Override
				public void windowClosing (WindowEvent e){
					serverRunnerUI.exitServer();
				}
			});
			serverRunnerUI.pack();
			serverRunnerUI.setVisible(true);
			serverRunnerUI.setLocationRelativeTo(null);
			serverRunnerUI.toFront();
			serverRunnerUI.setState(Frame.NORMAL);

			installSystemTray();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void installSystemTray() throws Exception {
		if (!SystemTray.isSupported()) {
			logger.error("SystemTray is not supported.");
			return;
		}

		PopupMenu menu = new PopupMenu();
		MenuItem mnuApp = new MenuItem(APP_TITLE);
		mnuApp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				serverRunnerUI.setVisible(true);
				serverRunnerUI.toFront();
				serverRunnerUI.setState(Frame.NORMAL);
			}
		});
		menu.add(mnuApp);

		menu.addSeparator();

		MenuItem mnuExit = new MenuItem("Exit");
		mnuExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				serverRunnerUI.exitServer();
			}
		});
		menu.add(mnuExit);

		TrayIcon trayIcon = new TrayIcon(getImage(), APP_TITLE, menu);
		trayIcon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, APP_TITLE);
			}
		});

		try {
			final SystemTray tray = SystemTray.getSystemTray();
			tray.add(trayIcon);
		} catch (AWTException e) {
			logger.error("TrayIcon could not be added due to the error: " + e.getMessage());
		}
	}

	private static Image getImage() throws HeadlessException {
		return new ImageIcon(
				ServerRunnerUI.class.getResource("images/logo_small.png"))
				.getImage();
	}
	
	/**
	 * insertLog
	 * 
	 * @param logLevel
	 * @param logMessage
	 * @param timeStamp
	 */
	public static void insertLog(final Level logLevel, final String logMessage,
			final long timeStamp) {
		serverRunnerUI.insertLog(logLevel, logMessage, timeStamp);
	}
}
