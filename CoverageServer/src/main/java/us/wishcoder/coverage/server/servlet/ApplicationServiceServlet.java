package us.wishcoder.coverage.server.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import us.wishcoder.coverage.server.schema.Coverage;
import us.wishcoder.coverage.server.schema.Project;
import us.wishcoder.coverage.server.schema.Report;
import us.wishcoder.coverage.server.schema.User;
import us.wishcoder.coverage.server.utils.CodeCoverageUtil;
import us.wishcoder.coverage.server.utils.HibernateUtil;

public class ApplicationServiceServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8031960862761713193L;

	private static Logger logger = Logger
			.getLogger(ApplicationServiceServlet.class);

	private static final String QUERY_TYPE_VALIDATE_LOGIN = "qtvl";
	private static final String QUERY_TYPE_UPDATE_USER = "qtus";
	private static final String QUERY_TYPE_GET_PROJECT_LIST = "qtgpl";
	private static final String QUERY_TYPE_GET_COVERAGE_LIST = "qtgcl";
	private static final String QUERY_TYPE_GET_REPORT_LIST = "qtgrl";

	private static final String QUERY_TYPE_SUPER_CREATE_ADMIN_USER = "qtscau";
	private static final String QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST = "qtsgaul";

	private static final String QUERY_TYPE_ADMIN_GET_SUBORDINATES = "qtags";
	private static final String QUERY_TYPE_ADMIN_CREATE_SUBORDINATE = "qtacs";
	private static final String QUERY_TYPE_ADMIN_SAVE_PROJECT = "qtasp";
	private static final String QUERY_TYPE_ADMIN_SAVE_REPORT = "qtasr";
	private static final String QUERY_TYPE_USER_LOGOUT = "qtlo";

	private static final String USER_TYPE_SUPER_ADMIN = "SUPER";
	private static final String USER_TYPE_ADMIN = "ADMIN";
	private static final String USER_TYPE_REGULAR = "REGULAR";

	private static final SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy-MM-dd @ hh:mm:ss:SSS a");

	private static final SimpleDateFormat reportDateFormatter = new SimpleDateFormat(
			"yyyy-MM-dd_hh-mm-ss-SSS_a");

	// location to store file upload
	private static final String UPLOAD_DIRECTORY = "upload";

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		allowAccessControlOrigin(request, response, getServletContext()
				.getInitParameter("allowAccessControlOrigin").trim());

		logger.info("[" + getUserName(request) + "] Request for '"
				+ getRequestType(request.getParameter("qtype")) + "'");

		if (QUERY_TYPE_VALIDATE_LOGIN.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			validateLogin(request, response);
		} else if (QUERY_TYPE_USER_LOGOUT.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			processLogout(request, response);
		} else if (QUERY_TYPE_UPDATE_USER.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			updateUser(request, response);
		} else if (QUERY_TYPE_GET_PROJECT_LIST.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			getProjectList(request, response);
		} else if (QUERY_TYPE_SUPER_CREATE_ADMIN_USER.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			saveAdminUser(request, response);
		} else if (QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST
				.equalsIgnoreCase(request.getParameter("qtype"))
				|| QUERY_TYPE_ADMIN_GET_SUBORDINATES.equalsIgnoreCase(request
						.getParameter("qtype"))) {
			getUserList(request, response, request.getParameter("qtype"));
		} else if (QUERY_TYPE_ADMIN_CREATE_SUBORDINATE.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			saveUser(request, response);
		} else if (QUERY_TYPE_ADMIN_SAVE_PROJECT.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			saveProject(request, response);
		} else if (QUERY_TYPE_GET_COVERAGE_LIST.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			getCoverageList(request, response);
		} else if (QUERY_TYPE_ADMIN_SAVE_REPORT.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			generateCoverageReport(request, response);
		} else if (QUERY_TYPE_GET_REPORT_LIST.equalsIgnoreCase(request
				.getParameter("qtype"))) {
			getCoverageReportList(request, response);
		}
	}

	protected void validateLogin(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();

		JSONArray json = new JSONArray();
		Session session = null;

		try {
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("userName",
					request.getParameter("username")));
			criteria.add(Restrictions.eq("userPassword",
					request.getParameter("password")));
			criteria.add(Restrictions.eq("active", true));

			@SuppressWarnings("unchecked")
			List<User> userList = (List<User>) criteria.list();
			if (userList == null || userList.size() == 0) {
				JSONObject jsono = new JSONObject();
				jsono.put("error", "Invalid Username and/or Password");
				json.put(jsono);
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			} else {
				for (User user : userList) {
					JSONObject jsono = new JSONObject();
					jsono.put("error", "");
					jsono.put("sessionid", request.getSession().getId());
					jsono.put("userid", user.getUserId());
					jsono.put("username", user.getUserName());
					jsono.put("usertype",
							(user.isSuperAdmin() ? USER_TYPE_SUPER_ADMIN
									: (user.isAdmin() ? USER_TYPE_ADMIN
											: USER_TYPE_REGULAR)));

					json.put(jsono);

					request.getSession().setAttribute(request.getSession().getId(), user);
				}
			}
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void processLogout(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();

		JSONArray json = new JSONArray();
		User user = (User) request.getSession().getAttribute(
				request.getParameter("sessionid"));
		if (user != null) {
			request.getSession().removeAttribute(
					request.getParameter("sessionid"));
		}

		JSONObject jsono = new JSONObject();
		jsono.put("error", "");
		jsono.put("username", user.getUserName());
		json.put(jsono);

		writer.write(json.toString());
		writer.flush();
		writer.close();
	}

	protected void saveAdminUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		try {
			boolean isDelete = false;

			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			User user = null;
			if (!"".equalsIgnoreCase(request.getParameter("userid"))
					&& (Integer.parseInt(request.getParameter("userid")) > 0)) {
				user = (User) session.get(User.class,
						Long.parseLong(request.getParameter("userid")));

				if (!"".equalsIgnoreCase(request.getParameter("username"))) {
					// update user
					user.setUserName(request.getParameter("username"));
					user.setUserPassword(request.getParameter("password"));
					user.setActive(true);
				} else {
					// delete user
					isDelete = true;
					user.setActive(false);
				}
			} else {
				// new user
				user = new User(request.getParameter("username"),
						request.getParameter("password"));
			}

			JSONObject jsono = new JSONObject();

			try {
				session.getTransaction().begin();
				session.save(user);
				session.getTransaction().commit();

				if (isDelete) {
					jsono.put("success", request.getParameter("username")
							+ " deleted");
				} else {
					jsono.put("success", request.getParameter("username")
							+ " saved");
				}

				jsono.put("error", "");
				jsono.put("userid", user.getUserId());
				jsono.put("username", user.getUserName());
				jsono.put("password", user.getUserPassword());
				jsono.put("active", user.getActive());
			} catch (Exception e) {
				if (isDelete) {
					jsono.put(
							"error",
							"Unable to delete user '"
									+ request.getParameter("username")
									+ "' due to the error:" + e.getMessage());
				} else {
					jsono.put(
							"error",
							"Unable to save user '"
									+ request.getParameter("username")
									+ "' due to the error:" + e.getMessage());
				}
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			}

			json.put(jsono);
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void getUserList(HttpServletRequest request,
			HttpServletResponse response, String type) throws ServletException,
			IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		try {
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			if (QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST.equalsIgnoreCase(type)) {
				// super user list
				Criteria criteria = session.createCriteria(User.class);
				criteria.add(Restrictions.isNull("manager"));

				@SuppressWarnings("unchecked")
				List<User> userList = (List<User>) criteria.list();
				// list always returns Error object as 0th
				// element
				// Put empty error Object
				JSONObject jsono = new JSONObject();
				jsono.put("error", "");
				json.put(jsono);

				for (User user : userList) {
					jsono = new JSONObject();
					jsono.put("userid", user.getUserId());
					jsono.put("active", user.getActive());
					jsono.put("username", user.getUserName());
					jsono.put("password", user.getUserPassword());

					json.put(jsono);
				}

			} else if (QUERY_TYPE_ADMIN_GET_SUBORDINATES.equalsIgnoreCase(type)) {
				// Admin users list

				User user = (User) session.get(User.class,
						Long.parseLong(request.getParameter("userid")));
				Set<User> subordinates = user.getSubordinates();

				// list always returns Error object as 0th
				// element
				// Put empty error Object
				JSONObject jsono = new JSONObject();
				jsono.put("error", "");
				json.put(jsono);

				for (User userTmp : subordinates) {
					jsono = new JSONObject();
					jsono.put("userid", userTmp.getUserId());
					jsono.put("active", userTmp.getActive());
					jsono.put("username", userTmp.getUserName());
					jsono.put("password", userTmp.getUserPassword());

					json.put(jsono);
				}

			}
		} catch (Exception e) {
			JSONObject jsono = new JSONObject();
			jsono.put(
					"error",
					"Unable to get user list due to the error: "
							+ e.getMessage());
			json.put(jsono);
			logger.error("[" + getUserName(request) + "] " + jsono.get("error"));
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void saveUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		boolean isDelete = false;
		try {
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			User user = null;
			if (!"".equalsIgnoreCase(request.getParameter("userid"))
					&& (Integer.parseInt(request.getParameter("userid")) > 0)) {
				user = (User) session.get(User.class,
						Long.parseLong(request.getParameter("userid")));

				if (!"".equalsIgnoreCase(request.getParameter("username"))) {
					// update user
					user.setUserName(request.getParameter("username"));
					user.setUserPassword(request.getParameter("password"));
					user.setActive(true);
				} else {
					// delete user
					isDelete = true;
					user.setActive(false);
				}
			} else {
				// new user
				// first find manager
				User manager = (User) session.get(User.class,
						Long.parseLong(request.getParameter("managerid")));
				user = new User(request.getParameter("username"),
						request.getParameter("password"));
				user.setManager(manager);
			}

			JSONObject jsono = new JSONObject();
			try {
				session.getTransaction().begin();
				session.save(user);
				session.getTransaction().commit();

				if (isDelete) {
					jsono.put("success", request.getParameter("username")
							+ " deleted");
				} else {
					jsono.put("success", request.getParameter("username")
							+ " saved");
				}

				jsono.put("error", "");
				jsono.put("userid", user.getUserId());
				jsono.put("username", user.getUserName());
				jsono.put("password", user.getUserPassword());
				jsono.put("active", user.getActive());
			} catch (Exception e) {
				if (isDelete) {
					jsono.put(
							"error",
							"Unable to delete user '"
									+ request.getParameter("username")
									+ "' due to the error:" + e.getMessage());
				} else {
					jsono.put(
							"error",
							"Unable to save user '"
									+ request.getParameter("username")
									+ "' due to the error:" + e.getMessage());
				}
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			}

			json.put(jsono);
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void updateUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		try {
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			User user = (User) session.get(User.class,
					Long.parseLong(request.getParameter("userid")));
			user.setUserPassword(request.getParameter("password"));
			user.setActive(true);

			JSONObject jsono = new JSONObject();

			try {
				session.getTransaction().begin();
				session.save(user);
				session.getTransaction().commit();

				jsono.put("error", "");
				jsono.put("success", request.getParameter("username")
						+ " updated");
			} catch (Exception e) {
				jsono.put(
						"error",
						"Unable to save user '"
								+ request.getParameter("username")
								+ "' due to the error:" + e.getMessage());
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			}

			json.put(jsono);
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void saveProject(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		boolean isDelete = false;
		try {
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			Project project = null;
			if (!"".equalsIgnoreCase(request.getParameter("projectid"))
					&& (Integer.parseInt(request.getParameter("projectid")) > 0)) {
				project = (Project) session.get(Project.class,
						Long.parseLong(request.getParameter("projectid")));
				if (!"".equalsIgnoreCase(request.getParameter("projectname"))) {
					// update project
					project.setProjectName(request.getParameter("projectname"));
					project.setProjectDesc(request.getParameter("projectdesc"));
					project.setActive(true);
				} else {
					// delete project
					isDelete = true;
					project.setActive(false);
				}
			} else {
				// new project
				// first find owner of this project
				User user = (User) session.get(User.class,
						Long.parseLong(request.getParameter("userid")));

				project = new Project(request.getParameter("projectname"),
						request.getParameter("projectdesc"));

				project.setProjectUser(user);
			}

			JSONObject jsono = new JSONObject();
			try {
				session.getTransaction().begin();
				session.save(project);
				session.getTransaction().commit();

				if (isDelete) {
					jsono.put("success", request.getParameter("projectname")
							+ " deleted");
				} else {
					jsono.put("success", request.getParameter("projectname")
							+ " saved");
				}

				jsono.put("error", "");
				jsono.put("projectid", project.getProjectId());
				jsono.put("projectname", project.getProjectName());
				jsono.put("projectdesc", project.getProjectDesc());
				jsono.put("active", project.getActive());
			} catch (Exception e) {
				if (isDelete) {
					jsono.put(
							"error",
							"Unable to delete project '"
									+ request.getParameter("projectname")
									+ " due to the error: " + e.getMessage());
				} else {
					jsono.put(
							"error",
							"Unable to save project '"
									+ request.getParameter("projectname")
									+ " due to the error: " + e.getMessage());
				}
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			}

			json.put(jsono);
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void getProjectList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		try {
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();

			boolean isRegularUser = false;
			User user = (User) session.get(User.class,
					Long.parseLong(request.getParameter("userid")));
			isRegularUser = !user.isAdmin();
			if (isRegularUser) {
				user = user.getManager(); // regular user will see projects of
											// this user
			}

			// super user list
			Criteria criteria = session.createCriteria(Project.class);
			criteria.add(Restrictions.eq("projectUser", user));
			if (isRegularUser) {
				criteria.add(Restrictions.eq("active", true));
			}

			@SuppressWarnings("unchecked")
			List<Project> projectList = (List<Project>) criteria.list();
			// list always returns Error object as 0th
			// element
			// Put empty error Object
			JSONObject jsono = new JSONObject();
			jsono.put("error", "");
			json.put(jsono);

			for (Project project : projectList) {
				jsono = new JSONObject();
				jsono.put("projectid", project.getProjectId());
				jsono.put("projectname", project.getProjectName());
				jsono.put("projectdesc", project.getProjectDesc());
				jsono.put("active", project.getActive());

				json.put(jsono);
			}
		} catch (Exception e) {
			JSONObject jsono = new JSONObject();
			jsono.put("error", "Unable to get project list due to the error: "
					+ e.getMessage());
			json.put(jsono);
			logger.error("[" + getUserName(request) + "] " + jsono.get("error"));
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void getCoverageList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		JSONObject jsono = new JSONObject();

		String userid = request.getParameter("userid");
		String projectid = request.getParameter("projectid");

		try {
			if ("".equals(userid) && "".equals(projectid)) {
				jsono.put(
						"error",
						"Unable to load coverage list due to missing required parameter user and/or project");
				json.put(jsono);
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			} else {
				try {
					SessionFactory factory = HibernateUtil.getSessionFactory();
					session = factory.openSession();

					User user = (User) session.get(User.class,
							Long.parseLong(userid));
					Project project = (Project) session.get(Project.class,
							Long.parseLong(projectid));

					if (user == null && project == null) {
						jsono.put(
								"error",
								"Unable to load coverage list due to missing required parameter user and/or project");
						json.put(jsono);
						logger.error("[" + getUserName(request) + "] "
								+ jsono.get("error"));
					} else {
						boolean isRegularUser = !user.isAdmin();

						// super user list
						Criteria criteria = session
								.createCriteria(Coverage.class);

						if (isRegularUser) {
							criteria.add(Restrictions.eq("coverageUser", user));
						}
						criteria.add(Restrictions
								.eq("coverageProject", project));

						@SuppressWarnings("unchecked")
						List<Coverage> coverageList = (List<Coverage>) criteria
								.list();
						// list always returns Error object as 0th
						// element
						// Put empty error Object
						jsono = new JSONObject();
						jsono.put("error", "");
						json.put(jsono);

						for (Coverage coverage : coverageList) {
							jsono = new JSONObject();
							jsono.put("coveragename",
									coverage.getCoverageName());
							jsono.put("coverageproject",
									project.getProjectName());
							jsono.put("coverageuser", user.getUserName());
							jsono.put("coveragedate", formatter.format(coverage
									.getDateModified()));

							json.put(jsono);
						}
					}
				} catch (Exception e) {
					jsono = new JSONObject();
					jsono.put("error",
							"Unable to get coverage list due to the error: "
									+ e.getMessage());
					json.put(jsono);
					logger.error("[" + getUserName(request) + "] "
							+ jsono.get("error"));
				}
			}
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void generateCoverageReport(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		try {
			String userid = request.getParameter("userid");
			String projectid = request.getParameter("projectid");
			String coveragename = request.getParameter("coveragename");
			String selectedcoverage = request.getParameter("selectedcoverage");

			JSONObject jsono = new JSONObject();
			if ("".equals(userid) || "".equals(projectid)
					|| "".equals(coveragename) || "".equals(selectedcoverage)) {
				jsono.put("error",
						"Unable to generate coverage report due to missing required fields ");
				json.put(jsono);
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			} else {
				try {
					SessionFactory factory = HibernateUtil.getSessionFactory();
					session = factory.openSession();

					User user = (User) session.get(User.class,
							Long.parseLong(userid));
					Project project = (Project) session.get(Project.class,
							Long.parseLong(projectid));

					if (user == null || project == null) {
						jsono.put("error",
								"Unable to generate coverage report due to missing user and/or project");
						json.put(jsono);
						logger.error("[" + getUserName(request) + "] "
								+ jsono.get("error"));
					} else {
						// print files
						String coveragePath = getServletContext().getRealPath(
								"")
								+ File.separator
								+ UPLOAD_DIRECTORY
								+ File.separator
								+ project.getProjectName().trim().toLowerCase()
										.replaceAll(" ", "_");

						String coverageName = coveragename.trim().toLowerCase()
								.replaceAll(" ", "_")
								+ "_" + reportDateFormatter.format(new Date());

						// insert into db
						Report report = new Report(coverageName);
						report.setReportUser(user);
						report.setReportProject(project);

						session.getTransaction().begin();
						session.save(report);
						session.getTransaction().commit();

						try {
							// merge selected coverage files
							CodeCoverageUtil.executeMergeTask(
									getUserName(request), getServletContext()
											.getRealPath(""), coveragePath,
									selectedcoverage, "merged.exec");

							// generate coverage report
							CodeCoverageUtil.executeReportTask(
									getUserName(request), getServletContext()
											.getRealPath(""), coverageName,
									"merged.exec", coveragePath, (coveragePath
											+ File.separator + report
											.getReportId()), "*.jar",
									"*sources.jar");

							jsono.put("error", "");
							jsono.put("success", "Coverage report '"
									+ coverageName + "' generated");
							jsono.put("reportname", coverageName);
							jsono.put("reportproject", project.getProjectName());
							jsono.put("reportdate",
									formatter.format(report.getDateModified()));

							json.put(jsono);

						} catch (Exception e) {
							// roll back report db entry
							session.getTransaction().begin();
							session.delete(report);
							session.getTransaction().commit();

							jsono = new JSONObject();
							jsono.put("error",
									"Unable to generate coverage report due to the error: "
											+ e.getMessage());
							json.put(jsono);
							logger.error("[" + getUserName(request) + "] "
									+ jsono.get("error"));
						}
					}

				} catch (Exception e) {
					jsono = new JSONObject();
					jsono.put("error",
							"Unable to generate coverage report due to the error: "
									+ e.getMessage());
					json.put(jsono);
					logger.error("[" + getUserName(request) + "] "
							+ jsono.get("error"));
				}
			}
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	protected void getCoverageReportList(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();
		JSONArray json = new JSONArray();
		Session session = null;

		JSONObject jsono = new JSONObject();

		String userid = request.getParameter("userid");
		String projectid = request.getParameter("projectid");

		try {
			if ("".equals(userid) && "".equals(projectid)) {
				jsono.put(
						"error",
						"Unable to load coverage report list due to the missing required parameter user and/or project");
				json.put(jsono);
				logger.error("[" + getUserName(request) + "] "
						+ jsono.get("error"));
			} else {
				try {
					SessionFactory factory = HibernateUtil.getSessionFactory();
					session = factory.openSession();

					User user = (User) session.get(User.class,
							Long.parseLong(userid));
					Project project = (Project) session.get(Project.class,
							Long.parseLong(projectid));

					if (user == null && project == null) {
						jsono.put(
								"error",
								"Unable to load coverage report list due to the missing required parameter user and/or project");
						json.put(jsono);
						logger.error("[" + getUserName(request) + "] "
								+ jsono.get("error"));
					} else {
						boolean isRegularUser = !user.isAdmin();
						if (isRegularUser) {
							// regular user will see projects
							// created by manager
							user = user.getManager();
						}

						// super user list
						Criteria criteria = session
								.createCriteria(Report.class);

						if (isRegularUser) {
							criteria.add(Restrictions.eq("reportUser", user));
						}
						criteria.add(Restrictions.eq("reportProject", project));

						@SuppressWarnings("unchecked")
						List<Report> reportList = (List<Report>) criteria
								.list();
						// list always returns Error object as 0th
						// element
						// Put empty error Object
						jsono = new JSONObject();
						jsono.put("error", "");
						json.put(jsono);

						for (Report report : reportList) {
							jsono = new JSONObject();
							jsono.put("reportname", report.getReportName());
							jsono.put("reportdate",
									formatter.format(report.getDateModified()));
							jsono.put("reportlink", UPLOAD_DIRECTORY
									+ "/"
									+ report.getReportProject()
											.getProjectName().trim()
											.toLowerCase()
											.replaceAll(" ", "_") + "/"
									+ report.getReportId() + "/index.html");
							jsono.put("coveragedate",
									formatter.format(report.getDateModified()));

							json.put(jsono);
						}
					}
				} catch (Exception e) {
					jsono = new JSONObject();
					jsono.put("error",
							"Unable to get coverage reeport list due to the error: "
									+ e.getMessage());
					json.put(jsono);
					logger.error("[" + getUserName(request) + "] "
							+ jsono.get("error"));
				}
			}
		} finally {
			if (session != null && session.isOpen())
				session.close();
			session = null;

			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}

	public static final void allowAccessControlOrigin(
			HttpServletRequest request, HttpServletResponse response,
			String allowAccessControlOrigin) {
		if ("true".equalsIgnoreCase(allowAccessControlOrigin)) {
			// get client's origin
			// String clientOrigin = request.getHeader("origin");

			// list of origins
			// List<String> incomingURLs = Arrays.asList(getServletContext()
			// .getInitParameter("incomingURLs").trim().split(","));

			response.setContentType("application/json");
			response.setHeader("Cache-control", "no-cache, no-store");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Expires", "-1");
			response.setHeader("Access-Control-Allow-Origin",
					request.getHeader("Origin"));
			response.setHeader("Access-Control-Allow-Methods", "POST");
			response.setHeader("Access-Control-Allow-Headers", "Content-Type");
			response.setHeader("Access-Control-Max-Age", "86400");
		}
	}

	public static String getUserName(HttpServletRequest request) {
		try {
			if (request.getSession().getAttribute(
					request.getParameter("sessionid")) == null) {
				if (request.getParameter("username") != null) {
					return request.getParameter("username");
				} else {
					return "default user";
				}
			} else {
				User user = (User) request.getSession().getAttribute(
						request.getParameter("sessionid"));
				return user.getUserName();
			}
		} catch (Exception e) {
			logger.error("Exception in 'getUSer' " + e.getMessage());
			return "error user";
		}
	}

	private String getRequestType(String reqType) {

		switch (reqType) {
		case QUERY_TYPE_VALIDATE_LOGIN:
			return "login validation";
		case QUERY_TYPE_UPDATE_USER:
			return "update user information";
		case QUERY_TYPE_GET_PROJECT_LIST:
			return "get project list";
		case QUERY_TYPE_GET_COVERAGE_LIST:
			return "get coverage list";
		case QUERY_TYPE_GET_REPORT_LIST:
			return "get coverage report list";
		case QUERY_TYPE_SUPER_CREATE_ADMIN_USER:
			return "update admin user information";
		case QUERY_TYPE_SUPER_GET_ADMIN_USER_LIST:
			return "get admin user list";
		case QUERY_TYPE_ADMIN_GET_SUBORDINATES:
			return "get user list";
		case QUERY_TYPE_ADMIN_CREATE_SUBORDINATE:
			return "create user";
		case QUERY_TYPE_ADMIN_SAVE_PROJECT:
			return "save project";
		case QUERY_TYPE_ADMIN_SAVE_REPORT:
			return "save report";
		case QUERY_TYPE_USER_LOGOUT:
			return "log out";
		default:
			return "";
		}
	}
}
