package us.wishcoder.coverage.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

public class CoverageReportServlet extends HttpServlet{
	private static final long serialVersionUID = 8715608978031633135L;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
			PrintWriter writer = response.getWriter();
			response.setContentType("application/json");
			JSONArray json = new JSONArray();
			JSONObject jsono = new JSONObject();
			jsono.put("url","http://localhost:8080/codecoverage");
			json.put(jsono);

			writer.write(json.toString());
			writer.flush();
			writer.close();
	}
}





