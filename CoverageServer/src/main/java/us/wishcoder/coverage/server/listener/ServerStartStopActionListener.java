package us.wishcoder.coverage.server.listener;

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import us.wishcoder.coverage.server.JettyServer;
import us.wishcoder.coverage.server.schema.User;
import us.wishcoder.coverage.server.utils.HibernateUtil;

public class ServerStartStopActionListener implements ActionListener{
	private static Logger logger = Logger.getLogger(ServerStartStopActionListener.class);
	
	private final JettyServer jettyServer;
	private final JButton buttonAdmin;
	
	public ServerStartStopActionListener(JettyServer jettyServer, JButton buttonAdmin){
		this.jettyServer = jettyServer;
		this.buttonAdmin = buttonAdmin;
	}	
	
	public void actionPerformed(ActionEvent actionEvent){
		JButton buttonStartStop = (JButton) actionEvent.getSource();
		if(jettyServer.isStarted()){
			buttonAdmin.setEnabled(false);
			buttonStartStop.setText("Stopping...");
			buttonStartStop.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			try{
				jettyServer.stop();
				buttonStartStop.setText("Start");
				logger.info("Code coverage server stopped.");
			}catch(Exception exception){
				logger.error("Jetty server initialization failed due to the error: " + exception.getMessage());
			}
			buttonStartStop.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));	
		} else if(jettyServer.isStopped()){
			buttonStartStop.setText("Starting...");
			buttonStartStop.setCursor(new Cursor(Cursor.WAIT_CURSOR));
			try{
				jettyServer.start();
				logger.info("Code coverage server started.");
				initCoverageDB();
				buttonAdmin.setEnabled(true);
				buttonStartStop.setText("Stop");
			}catch(Exception exception){
				logger.error("Code coverage server initialization failed due to the error: " + exception.getMessage());
			}		
			buttonStartStop.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));				
		}		
	}
	
	private void initCoverageDB(){
		logger.info("Initializing code coverage database...");
		Session session = null;
		
		try{
			SessionFactory factory = HibernateUtil.getSessionFactory();
			session = factory.openSession();
			
			Criteria criteria = session.createCriteria(User.class);
			criteria.add(Restrictions.eq("userName","admin"));
			
			@SuppressWarnings("unchecked")
			List<User> userList = (List<User>) criteria.list();
			if(userList != null && userList.size() == 1){
				// admin user exists
			} else {
				// insert admin user
				session.getTransaction().begin();
				User user = new User("admin", System.getProperty("adminpwd", "welcome"));
				session.save(user);
				user.setManager(user);
				session.save(user);
				session.getTransaction().commit();
			}	
			logger.info("Code coverage database initialized.");			
		}catch(Exception exception){
			logger.error("Code coverage database  initialization failed due to the error: " + exception.getMessage());
		}finally{
			if(session != null && session.isOpen()){
				session.close();
			}
			session = null;
		}		
	}	
}




