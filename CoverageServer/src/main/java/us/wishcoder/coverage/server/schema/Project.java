package us.wishcoder.coverage.server.schema;


import java.sql.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "PROJECT")
public class Project{
	@Id
	@GeneratedValue
	@Column(name = "project_id")
	private Long projectId;
	
	@Column(name = "project_name", length=64, unique=true, nullable=false)
	private String projectName;

	@Column(name = "project_desc", length=256, nullable=false)
	private String projectDesc;
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private User projectUser;

	@OneToMany(mappedBy="coverageProject")
	private Set<Coverage> coverage;
	
	@OneToMany(mappedBy="reportProject")
	private Set<Report> reports;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "date_modified")
	private Date dateModified;
	
	public Project(){
		super();
		this.active = true;
		this.dateModified = new Date(System.currentTimeMillis());
	}	
	
	public Project(String projectName, String projectDesc){
		this();
		this.projectName = projectName;
		this.projectDesc = projectDesc;
	}	
	
	public Long getProjectId(){
		return this.projectId;
	}
	
	public void setProjectId(Long projectId){
		this.projectId = projectId;
	}
	
	public String getProjectName(){
		return this.projectName;
	}
	
	public void setProjectName(String projectName){
		this.projectName= projectName;
	}	

	public String getProjectDesc(){
		return this.projectDesc;
	}
	
	public void setProjectDesc(String projectDesc){
		this.projectDesc= projectDesc;
	}
	
	
	public User getProjectUser(){
		return this.projectUser;
	}	
	
	public void setProjectUser(User projectUser){
		this.projectUser = projectUser;
	}	
	
	public Set<Coverage> getCoverage(){
		return this.coverage;
	}
	
	public void setCoverage(Set<Coverage> coverage){
		this.coverage = coverage;
	}
	
	public Set<Report> getReports(){
		return this.reports;
	}
	
	public void setReports(Set<Report> reports){
		this.reports = reports;
	}
	
	public Boolean getActive(){
		return this.active;
	}

	public void setActive(Boolean active){
		this.active = active;
	}
	
	public Date getDateModified(){
		return this.dateModified;	
	}	
	
	public void setDateModified(Date dateModified){
		this.dateModified = dateModified;	
	}
}




