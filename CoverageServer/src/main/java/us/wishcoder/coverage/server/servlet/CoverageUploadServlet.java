package us.wishcoder.coverage.server.servlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONArray;
import org.json.JSONObject;

import us.wishcoder.coverage.server.schema.Coverage;
import us.wishcoder.coverage.server.schema.Project;
import us.wishcoder.coverage.server.schema.User;
import us.wishcoder.coverage.server.utils.HibernateUtil;

public class CoverageUploadServlet extends HttpServlet {
	private static final long serialVersionUID = -2150409084924165731L;
	private static Logger logger = Logger
			.getLogger(CoverageUploadServlet.class);

	private static final String UPLOAD_DIRECTORY = "upload";

	private static final int MEMORY_THRESHOLD = 1024 * 1024 * 3; // 3MB
	private static final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	private static final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

	private static final SimpleDateFormat formatter = new SimpleDateFormat(
			"yyyy-MM-dd @ hh:mm:ss:SSS a");

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		PrintWriter writer = response.getWriter();

		if (!ServletFileUpload.isMultipartContent(request)) {
			String msg = "Error: Form must have enctype=multipart/form-data";
			writer.println(msg);
			logger.error("[" + ApplicationServiceServlet.getUserName(request) + "] " + msg);
			return;
		}
		ApplicationServiceServlet.allowAccessControlOrigin(request, response, getServletContext().getInitParameter("allowAccessControlOrigin").trim());
		JSONArray json = new JSONArray();

		// configure upload settings
		DiskFileItemFactory factory = new DiskFileItemFactory();
		// set memory threashold beyond which files are stored in disk
		factory.setSizeThreshold(MEMORY_THRESHOLD);
		// set temporary location to save files
		factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

		ServletFileUpload upload = new ServletFileUpload(factory);

		// set max size of upload files
		upload.setFileSizeMax(MAX_FILE_SIZE);

		// set maximum size of request (include file + form data)
		upload.setSizeMax(MAX_REQUEST_SIZE);

		// Construct directory path to store upload file
		// This path is relaticve to the application's root directory
		String uploadPath = getServletContext().getRealPath("")
				+ File.separator + UPLOAD_DIRECTORY;

		try {
			List<FileItem> formItems = upload.parseRequest(request);
			if (formItems != null && formItems.size() > 0) {
				// iterate over form fields
				String userid = "";
				String projectid = "";
				String type = "";

				for (FileItem item : formItems) {
					if ("userid".equalsIgnoreCase(item.getFieldName())) {
						InputStream stream = item.getInputStream();
						userid = Streams.asString(stream);
					}

					if ("projectid".equalsIgnoreCase(item.getFieldName())) {
						InputStream stream = item.getInputStream();
						projectid = Streams.asString(stream);
					}

					if ("type".equalsIgnoreCase(item.getFieldName())) {
						InputStream stream = item.getInputStream();
						type = Streams.asString(stream);
					}
				}

				JSONObject jsono = new JSONObject();
				if ("".equals(userid) || "".equals(projectid)
						|| "".equals(type)) {
					String msg = "Unable to uplpad coverage file dute to the required missing fields";
					jsono.put("error",msg);
					json.put(jsono);
					logger.error("[" + ApplicationServiceServlet.getUserName(request) + "] " + msg);
				} else {
					for (FileItem item : formItems) {
						// process only fields that not form field
						if (!item.isFormField()) {
							Session session = null;
							try {
								SessionFactory sessionFactory = HibernateUtil
										.getSessionFactory();
								session = sessionFactory.openSession();

								User user = (User) session.get(User.class,
										Long.parseLong(userid));
								Project project = (Project) session.get(
										Project.class,
										Long.parseLong(projectid));

								if (user == null || project == null) {
									String msg = "Unable to upload coverage file due to the missing user or ptoject information";
									jsono.put("error",msg);
									json.put(jsono);
									logger.error("[" + ApplicationServiceServlet.getUserName(request) + "] " + msg);
								} else {
									File uploadDir = new File(uploadPath);
									if (!uploadDir.exists()) {
										uploadDir.mkdir();
									}

									if (uploadDir.exists()) {
										uploadPath = uploadPath
												+ File.separator
												+ project.getProjectName()
														.trim().toLowerCase()
														.replaceAll(" ", "_");

										uploadDir = new File(uploadPath);
										if (!uploadDir.exists()) {
											uploadDir.mkdir();
										}

										String fileName = "";
										String filePath = "";

										if ("source".equalsIgnoreCase(type
												.trim())) {
											// admin is uploading project jars
											fileName = new File(item.getName())
													.getName();
											filePath = uploadPath
													+ File.separator + fileName;
											File storeFile = new File(filePath);

											try {
												if (storeFile.exists()) {
													storeFile.delete();
												}
											} catch (Exception e) {
												// ignore error
											}

											item.write(storeFile);

											jsono.put("error", "");
											jsono.put("success", fileName
													+ " uploaded");
										} else {
											// user is uploading coverage file
											// for selected project

											fileName = user.getUserName()
													.toLowerCase().trim()
													+ "_"
													+ project
															.getProjectName()
															.trim()
															.toLowerCase()
															.replaceAll(" ",
																	"_")
													+ "_"
													+ new File(item.getName())
															.getName();

											filePath = uploadPath
													+ File.separator + fileName;
											File storeFile = new File(filePath);

											try {
												if (storeFile.exists()) {
													storeFile.delete();
												}
											} catch (Exception e) {
												// ignore error
											}

											item.write(storeFile);

											// check if this coveraage file
											// already exists
											// in db. update if exists
											Coverage coverage = null;
											Criteria criteria = session
													.createCriteria(Coverage.class);
											criteria.add(Restrictions.eq(
													"coverageName", fileName));
											@SuppressWarnings("unchecked")
											List<Coverage> coverageList = (List<Coverage>) criteria
													.list();
											if (coverageList != null
													&& coverageList.size() > 0) {
												// upload coverage
												coverage = coverageList.get(0);
												coverage.setDateModified(new Date(System.currentTimeMillis()));
												jsono.put("type", "update");
											} else {
												// new coverage
												coverage = new Coverage(
														fileName);
												coverage.setCoverageUser(user);
												coverage.setCoverageProject(project);
												jsono.put("type", "create");
											}

											session.getTransaction().begin();
											session.save(coverage);
											session.getTransaction().commit();

											jsono.put("error", "");
											jsono.put("succesas", fileName
													+ " uploaded");
											jsono.put("coverageproject",
													project.getProjectName());
											jsono.put("coveragename",
													coverage.getCoverageName());
											jsono.put("coverageuser",
													user.getUserName());
											jsono.put("coveragedate", formatter
													.format(coverage
															.getDateModified()));
										}
									}
								}
							} catch (Exception e) {
								String msg = "Unable to upload coverage file due to the error: "+ e.getMessage();
								jsono.put("error",msg);
								logger.error("[" + ApplicationServiceServlet.getUserName(request) + "] " + msg);
							} finally {
								if (session != null && session.isOpen())
									session.close();
								session = null;
							}
							json.put(jsono);
						}
					}
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			writer.write(json.toString());
			writer.flush();
			writer.close();
		}
	}
}
