package us.wishcoder.coverage.server.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import us.wishcoder.coverage.server.CoverageServerMain;
import us.wishcoder.coverage.server.JettyServer;
import us.wishcoder.coverage.server.listener.ServerStartStopActionListener;

public class ServerRunnerUI extends JFrame {
	private static final long serialVersionUID = -5277154122332645070L;
	private static Logger logger = Logger.getLogger(ServerRunnerUI.class);

	private final JettyServer jettyServer;
	private JButton btnStartStop;

	private JTextPane logConsole;
	private StyledDocument logConsoleTextPaneStyleDocument;
	private Style logConsoleTextPaneErrorStyle;
	private Style logConsoleTextPaneTimestampStyle;
	private JScrollPane logConsoleScrollPane;

	private static final SimpleDateFormat dateFormatLogTimestamp = new SimpleDateFormat(
			"MM/dd/yyyy HH:mm:ss:SSS");
	private final JPanel appPanel;

	public ServerRunnerUI(final JPanel appPanel, final JettyServer jettyServer,
			String uiTitle) throws Exception {
		super(uiTitle);
		this.appPanel = appPanel;
		this.jettyServer = jettyServer;

		setPreferredSize(new Dimension(500, 360));
		setResizable(false);

		//setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getContentPane().add(initUI(appPanel));

		List<Image> iconImages = new ArrayList<Image>();
		iconImages.add(new ImageIcon(ServerRunnerUI.class
				.getResource("images/logo_small.png")).getImage());
		iconImages.add(new ImageIcon(ServerRunnerUI.class
				.getResource("images/logo.png")).getImage());
		setIconImages(iconImages);

//		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
//			public void run() {
//				exitServer();
//			}
//		}, "Stop Jetty Server"));
	}

	private JPanel initUI(final JPanel appPanel) throws Exception {
		appPanel.add(getLogContainer());
		appPanel.add(getButtonPanel());
		return appPanel;
	}

	private JScrollPane getLogContainer() {
		logConsole = new JTextPane();
		logConsoleTextPaneStyleDocument = logConsole.getStyledDocument();
		logConsoleTextPaneErrorStyle = logConsole.addStyle("Error", null);
		StyleConstants.setForeground(logConsoleTextPaneErrorStyle, Color.red);
		StyleConstants.setBold(logConsoleTextPaneErrorStyle, true);
		logConsoleTextPaneTimestampStyle = logConsole.addStyle("Timestamp",
				null);
		StyleConstants.setForeground(logConsoleTextPaneTimestampStyle,
				Color.decode("#191970"));
		StyleConstants.setBold(logConsoleTextPaneTimestampStyle, true);

		// logConsole.setLineWrap(true);
		// logConsole.setWrapStyleWord(true);
		logConsole.setBackground(Color.decode("#FFFFFF")); // "#FFFACD" - Light
															// Yellow
		logConsole.setEditable(false);

		// Create the scroll pane and add the
		// logConsole to it.
		logConsoleScrollPane = new JScrollPane(logConsole);
		logConsoleScrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);
		Dimension newPrefferedSize = new Dimension(
				getPreferredSize().width - 25, getPreferredSize().height - 75);
		logConsoleScrollPane.setPreferredSize(newPrefferedSize);
		logConsoleScrollPane.setBorder(BorderFactory
				.createTitledBorder("Messages"));

		return logConsoleScrollPane;
	}

	private JPanel getButtonPanel() {
		JPanel buttonPanel = new JPanel();
		final JButton buttonAdmin = new JButton("Admin Console...");
		buttonAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					openAdmin();
				} catch (Exception ex) {
					logger.error("Unable to open admin console due to the error: "
							+ ex.getMessage());
				}
			}
		});
		buttonAdmin.setEnabled(false);

		btnStartStop = new JButton("Start");
		btnStartStop.addActionListener(new ServerStartStopActionListener(
				jettyServer, buttonAdmin));
		buttonPanel.add(btnStartStop);

		final JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logConsole.setText("");
			}
		});
		buttonPanel.add(btnClear);
		buttonPanel.add(buttonAdmin);

		final JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				exitServer();
			}
		});
		buttonPanel.add(btnExit);

		return buttonPanel;
	}

	private void openAdmin() throws Exception {
		Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop()
				: null;
		if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			String urlStr = "http://localhost:" + jettyServer.getPort()
					+ CoverageServerMain.APP_CONTEXT_PATH;
			desktop.browse(new URI(urlStr));

			logger.info("Open link: " + urlStr);
		}
	}

	public void exitServer() {
		try {
			if (jettyServer.isStarted()) {
				jettyServer.stop();
			}
			jettyServer.destroyServer();
		} catch (Exception exception) {
			exception.printStackTrace();
		}finally{
			logger.info("Shutdown code coverage server.");
			System.exit(1);
		}
	}

	/**
	 * insertLog
	 * 
	 * @param logLevel
	 * @param logMessage
	 * @param timeStamp
	 */
	public void insertLog(final Level logLevel, final String logMessage,
			final long timeStamp) {

		if (logConsoleScrollPane != null) {
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						String logTimestampStr = (dateFormatLogTimestamp
								.format(new Date(timeStamp)) + " - ");

						Style logStyle = null;
						if (logLevel == Level.ERROR) {
							logStyle = logConsoleTextPaneErrorStyle;
						} else {
							if (("" + logMessage).toLowerCase().indexOf(
									"total execution time") != -1
									|| ("" + logMessage).toLowerCase().indexOf(
											"processing (") != -1) {
								logStyle = logConsoleTextPaneTimestampStyle;
							}
						}

						logConsoleTextPaneStyleDocument.insertString(
								logConsoleTextPaneStyleDocument.getLength(),
								logTimestampStr,
								logConsoleTextPaneTimestampStyle);

						logConsoleTextPaneStyleDocument.insertString(
								logConsoleTextPaneStyleDocument.getLength(),
								logMessage + "\n", logStyle);
						logConsole.setCaretPosition(logConsole.getText()
								.length() - 1);
					} catch (Exception e) {
						// ignore
					}
				}
			});
		}
	}
}
