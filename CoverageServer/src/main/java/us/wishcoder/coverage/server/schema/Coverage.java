package us.wishcoder.coverage.server.schema;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "COVERAGE")
public class Coverage{
	@Id
	@GeneratedValue
	@Column(name = "coverage_id")
	private Long coverageId;
	
	@Column(name = "coverage_name", length=256, unique=true, nullable=false)
	private String coverageName;

	@ManyToOne
	@JoinColumn(name="user_id")
	private User coverageUser;

	@ManyToOne
	@JoinColumn(name="project_id")
	private Project coverageProject;	
	
	@Column(name = "date_modified")
	private Date dateModified;
	
	public Coverage(){
		super();
		this.dateModified = new Date(System.currentTimeMillis());
	}	
	
	public Coverage(String coverageName){
		this();
		this.coverageName = coverageName;
	}	
	
	public Long getCoverageId(){
		return this.coverageId;
	}
	
	public void setCoverageId(Long coverageId){
		this.coverageId = coverageId;
	}
	
	public String getCoverageName(){
		return this.coverageName;
	}
	
	public void setCoverageName(String coverageName){
		this.coverageName= coverageName;
	}	
	
	public User getCoverageUser(){
		return this.coverageUser;
	}	
	
	public void setCoverageUser(User coverageUser){
		this.coverageUser = coverageUser;
	}	
	
	public Project getCoverageProject(){
		return this.coverageProject;
	}
	
	public void setCoverageProject(Project coverageProject){
		this.coverageProject = coverageProject;
	}
	
	public Date getDateModified(){
		return this.dateModified;	
	}	
	
	public void setDateModified(Date dateModified){
		this.dateModified = dateModified;	
	}
}




