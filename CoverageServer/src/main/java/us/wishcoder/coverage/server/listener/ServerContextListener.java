package us.wishcoder.coverage.server.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;

public class ServerContextListener implements ServletContextListener{

	private static Logger logger = Logger.getLogger(ServerContextListener.class);
	
	public void contextDestroyed(ServletContextEvent servletContextEvent){
		logger.info("ServerContextListener contextDestroyed");
	}

	public void contextInitialized(ServletContextEvent servletContextEvent){
		logger.info("ServerContextListener contextInitialized	");
	}
}


