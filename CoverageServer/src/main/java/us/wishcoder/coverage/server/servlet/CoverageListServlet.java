package us.wishcoder.coverage.server.servlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class CoverageListServlet extends HttpServlet{
	private static final long serialVersionUID = 4404526347126901667L;
	private static Logger logger = Logger.getLogger(CoverageListServlet.class);
	
	// location to store file upload
	private static final String UPLOAD_DIRECTORY = "upload";
	private static final String FILE_SUFFIX = "exec";
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
			PrintWriter writer = response.getWriter();
			response.setContentType("application/json");
			JSONArray json = new JSONArray();
			
			String uploadPath = getServletContext().getRealPath("") + File.separator + UPLOAD_DIRECTORY;
			logger.info("[" + ApplicationServiceServlet.getUserName(request) + "] Uploading coverage file to: " + uploadPath);
			
			try{
				File directory = new File(uploadPath);
				File[] fList = directory.listFiles();
				
				for(File file : fList){
					if(file.isFile() && getSuffix(file.getName()).equals(FILE_SUFFIX) ){
						JSONObject jsono = new JSONObject();
						jsono.put("name", file.getName());
						jsono.put("path", file.getAbsolutePath());
						
						json.put(jsono);
					}
				}
			}catch(Exception ex){
				JSONObject jsono = new JSONObject();
				jsono.put("error", "Unable to upload coverage file due to the error: " + ex.getMessage());
				json.put(jsono);
				logger.error("[" + ApplicationServiceServlet.getUserName(request) + "] " + jsono.get("error"));
			}finally{
				writer.write(json.toString());
				writer.flush();
				writer.close();
			}
	}
	
	private String getSuffix(String fileName){
		String suffix = "";
		int pos = fileName.lastIndexOf(".");
		if(pos > 0 && pos < fileName.length() - 1){
			suffix = fileName.substring(pos + 1);
		}
		return suffix;
	}
}





