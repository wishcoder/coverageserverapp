package tests;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import us.wishcoder.coverage.server.schema.Coverage;
import us.wishcoder.coverage.server.schema.Project;
import us.wishcoder.coverage.server.schema.Report;
import us.wishcoder.coverage.server.schema.User;
import us.wishcoder.coverage.server.utils.HibernateUtil;

public class TestHibernate{
	
	public TestHibernate(){
		
	}
	
	public static void main(String[] args){
		SessionFactory factory = HibernateUtil.getSessionFactory();
		Session session = factory.openSession();
		session.beginTransaction();
		
		//getUserByUserName(session, "admin", "welcome");
		
		// insert Admin user
		insertUser(session, true);
		
		// insert subordinates
		//User user = (User) session.get(User.class, 2L);
		// insertSubordinates(session, user);
		
		// insert project for admin user
		// insertProject(session, user);
		
		// printSubordinates(user);
		
		// User user = session.getUser(User.class, 3L);
		// Project project = (Project) session.get(Project.class, 1L);
		// insertCoverage(session, user, project);
		
		// listCoverageByUser(session, user);
		// listCoverageByProject(session, project);

		// insertReport(session, user, project);
		
		session.getTransaction().commit();	
		session.close();
		factory.close();
	}
	
	private static void insertUser(Session session, boolean isAdmin){
		User user = new User("admin", "welcome");
		session.save(user);
		
		if(isAdmin){
			user.setManager(user);
			session.save(user);
		}	
	}	
	
	private static void insertSubordinates(Session session, User user){
		User user1 = new User("ajsingh1", "welcome");
		User user2 = new User("ajsingh2", "welcome");
		
		user1.setManager(user);
		user2.setManager(user);
		
		session.save(user1);
		session.save(user2);
	}	
	
	private static void insertProject(Session session, User user){
		Project project = new Project("TestApp", "Test App Description");
		project.setProjectUser(user);
		session.save(project);
	}

	private static void printSubordinates(User user){
			Set<User> subordinates = user.getSubordinates();
			for(User subordinate : subordinates){
				System.out.println("Name: " + subordinate.getUserName());
			}	
	}	
	
	private static void getUserByUsername(Session session, String username, String password){
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("userName", username));
		criteria.add(Restrictions.eq("userPassword", password));
		criteria.add(Restrictions.eq("active", true));
		
		List<User> userList = (List<User>) criteria.list();
		for(User user : userList){
			System.out.println("Name: " + user.getUserName());
		}
	}
	
	private static void insertCoverage(Session session, User user, Project project){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd @ hh:mm:ss:SSS a");
		
		Coverage coverage = new Coverage(project.getProjectName() + "[" + formatter.format(new Date()) + "]");
		coverage.setCoverageProject(project);
		coverage.setCoverageUser(user);
		session.save(coverage);
	}	
	
	private static void listCoverageByUser(Session session, User user){
		Set<Coverage> coverageList = user.getCoverage();
		for(Coverage coverage : coverageList){
			System.out.println("Name: " + coverage.getCoverageName());
		}
	}
	
	private static void listCoverageByProject(Session session, Project project){
		Set<Coverage> coverageList = project.getCoverage();
		for(Coverage coverage : coverageList){
			System.out.println("Name: " + coverage.getCoverageName());
		}
	}
	
	private static void insertReport(Session session, User user, Project project){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd @ hh:mm:ss:SSS a");
		
		Report report = new Report(project.getProjectName() + "[" + formatter.format(new Date()) + "]");
		report.setReportProject(project);
		report.setReportUser(user);
		session.save(report);
	}
	
	private static void listReportByUser(Session session, User user){
		Set<Report> reportList = user.getReports();
		for(Report report : reportList){
			System.out.println("Name: " + report.getReportName());
		}
	}
	
	private static void listReportByProject(Session session, Project project){
		Set<Report> reportList = project.getReports();
		for(Report report : reportList){
			System.out.println("Name: " + report.getReportName());
		}
	}
}

